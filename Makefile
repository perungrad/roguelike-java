.PHONY: compile run

all: compile run

compile:
	javac --class-path=src/asciiPanel.jar:src src/rltut/screens/*.java
	javac --class-path=src/asciiPanel.jar:src src/rltut/*.java
run:
	java -Xmx100m --class-path=./src/asciiPanel.jar:./src rltut.ApplicationMain

